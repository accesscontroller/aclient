package cmd

import (
	"fmt"
	"strings"

	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

// supportedResources contains sorted slice of supported resources.
var supportedResources = []string{
	"accessgroup2externalgrouplistrelations",
	"accessgroup2webresourcecategorylistrelations",
	"accessgroups",
	"externalgroup2externaluserlistrelations",
	"externalgroups",
	"externalsessionssources",
	"externaluser2externalgrouplistrelations",
	"externalusersessions",
	"externalusers",
	"externalusersgroupssources",
	"webresources",
	"webresourcecategories",
}

var supportedResourcesFormatted = func() string {
	var sb strings.Builder

	_, _ = sb.WriteString("Supported resource kinds are:\n")

	for i := range supportedResources {
		_, _ = sb.WriteString(fmt.Sprintf("  - %s\n", supportedResources[i]))
	}

	return sb.String()
}()

var supportedResourcesToStorageResources = map[string]storage.ResourceKindT{
	"accessgroup2externalgrouplistrelations":       storage.AccessGroup2ExternalGroupListV1RelationsKind,
	"accessgroup2webresourcecategorylistrelations": storage.AccessGroup2WebResourceCategoryListV1RelationsKind,
	"accessgroups": storage.AccessGroupKind,
	"externalgroup2externaluserlistrelations": storage.ExternalGroup2ExternalUserListV1RelationsKind,
	"externalgroups":                          storage.ExternalGroupKind,
	"externalsessionssources":                 storage.ExternalSessionsSourceKind,
	"externaluser2externalgrouplistrelations": storage.ExternalUser2ExternalGroupListV1RelationsKind,
	"externalusersessions":                    storage.ExternalUserSessionKind,
	"externalusers":                           storage.ExternalUserKind,
	"externalusersgroupssources":              storage.ExternalUsersGroupsSourceKind,
	"webresources":                            storage.WebResourceKind,
	"webresourcecategories":                   storage.WebResourceCategoryKind,
}
