/*
Copyright © 2020 Dmitrii Ermakov <demonihin@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
package cmd

import (
	"crypto/tls"
	"fmt"
	"os"
	"strings"

	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/accesscontroller/aclient/pkg/configuration"
	"gitlab.com/accesscontroller/cclient.v1"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

var cfgFile string

var apiClient *cclient.CClient

var (
	logger    *zap.Logger
	logLevel  zapcore.Level = zap.PanicLevel
	sLogLevel string
)

// rootCmd represents the base command when called without any subcommands.
var rootCmd = &cobra.Command{
	Use:   "aclient",
	Short: "A console client to interact with Access Controller Server",
	Long:  ``,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	//	Run: func(cmd *cobra.Command, args []string) { },
	PersistentPreRunE: func(cmd *cobra.Command, args []string) error {
		// Parse configuration.
		cfg, err := configuration.Parse(viper.GetViper())
		if err != nil {
			return err
		}

		// Initialize CClient.
		apiClient, err = cclient.NewCustomConfig(cclient.ServerConfig{
			URL: cfg.APIServer.URL,
			TLS: &tls.Config{
				RootCAs:            cfg.APIServer.CACert,
				Certificates:       []tls.Certificate{cfg.APIServer.Client},
				MinVersion:         tls.VersionTLS12,
				InsecureSkipVerify: cfg.APIServer.InsecureConnection, // nolint:gosec
			},
			Timeout: cfg.APIServer.RequestTimeout,
			APIKey:  cfg.APIServer.APIKey,
		})
		if err != nil {
			return err
		}

		return nil
	},
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.
	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.aclient.yaml)")

	rootCmd.PersistentFlags().StringVar(&sLogLevel, "log-level", "panic",
		"set required log level from [debug, info, warn, error, panic]")

	cobra.OnInitialize(initConfig, initLogger)
}

func initLogger() {
	// Initializer Logger.
	switch strings.ToLower(sLogLevel) {
	case "debug":
		logLevel = zap.DebugLevel
	case "info":
		logLevel = zap.InfoLevel
	case "warn":
		logLevel = zap.WarnLevel
	case "error":
		logLevel = zap.ErrorLevel
	case "panic":
		logLevel = zap.PanicLevel
	default:
		panic(fmt.Sprintf("log level %q is not supported\n", sLogLevel))
	}

	core := zapcore.NewCore(zapcore.NewConsoleEncoder(zap.NewProductionEncoderConfig()),
		zapcore.AddSync(rootCmd.OutOrStdout()), logLevel)

	logger = zap.New(core)
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		// Search config in home directory with name ".aclient" (without extension).
		viper.AddConfigPath(home)
		viper.SetConfigName(".aclient")
	}

	viper.AutomaticEnv() // read in environment variables that match

	// Try to read a config.
	if err := viper.ReadInConfig(); err != nil {
		fmt.Println(fmt.Errorf("can not find and parse config file: %w", err))

		os.Exit(1)
	}
}
