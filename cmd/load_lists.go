/*
Copyright © 2020 Dmitrii Ermakov <demonihin@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
package cmd

import (
	"errors"
	"os"

	loadlists "gitlab.com/accesscontroller/aclient/pkg/load_lists"
	"go.uber.org/zap"

	"github.com/spf13/cobra"
)

var ErrLoadListsArgs = errors.New("invalid load-lists command arguments")

// loadListsCmdFlags load-lists command configuration.
var loadListsCmdFlags struct {
	simulate bool

	outputFormat string

	inputDir string
}

// loadListsCmd represents the loadLists command.
var loadListsCmd = &cobra.Command{
	Use:   "load-lists -i <path/to/directory>",
	Short: "loads WebResourceV1s in their WebResourceCategoryV1s",
	Long: `Call the command with input directory path and it will update WebResourceV1s and WebResourceCategoryV1s
on API Server replacing all the existing categories and resources.
The files structure in the directory must be:
directory/
  category1/
	domains
	urls
	ips
  category2/
	domains
	urls
	ips

All absent files are silently skipped.`,
	Example: `  # Loads all WebResourceV1s and WebResourceCategoryV1s.
  aclient load-lists -i lists_dir`,
	Run: func(cmd *cobra.Command, args []string) {
		if err := loadlists.Load(apiClient, logger, loadListsCmdFlags.simulate, loadListsCmdFlags.inputDir, "/", "-"); err != nil {
			logger.Error("Load error", zap.Error(err))

			os.Exit(1)
		}
	},
}

func init() {
	rootCmd.AddCommand(loadListsCmd)

	loadListsCmd.Flags().StringVarP(&loadListsCmdFlags.inputDir, "input", "i", "",
		"input directory to load WebResourceV1s and WebResourceCategoryV1s from (required).")

	loadListsCmd.Flags().StringVarP(
		&loadListsCmdFlags.outputFormat, "output", "o", "",
		"output format for the load result. Supported values are: json, yaml.")

	loadListsCmd.Flags().BoolVar(&loadListsCmdFlags.simulate, "simulate", false, "if set then the command does not change API Server content.")

	if err := loadListsCmd.MarkFlagRequired("input"); err != nil {
		panic(err)
	}
}
