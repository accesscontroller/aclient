/*
Copyright © 2020 Dmitrii Ermakov <demonihin@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
package cmd

import (
	"errors"
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
	"gitlab.com/accesscontroller/aclient/pkg/get"
	"gitlab.com/accesscontroller/aclient/pkg/printer"
)

var ErrGetArgs = errors.New("invalid get command arguments")

// getCmdFlags represents the get command run flags.
var getCmdFlags struct {
	outputFormat string
}

// getCmdArgsT contains parsed get command arguments.
type getCmdArgsT struct {
	kind   storage.ResourceKindT
	source storage.ResourceNameT
	names  []storage.ResourceNameT
}

// getCmd represents the get command.
var getCmd = &cobra.Command{
	Aliases: []string{"g"},
	Use:     "get <kind> [source] [name]",
	Short:   "Loads and returns a resource",
	Long: `Call the command with a resource kind and (optionally) a name to load a resource.

` + supportedResourcesFormatted,
	Example: `  # Loads an AccessGroupV1 named group-1 and prints its default representation.
  aclient get accessgroups group-1
  
  # Loads an AccessGroupV1 named group-1 and prints its JSON representation.
  aclient get accessgroups group-1 -o json

  # Loads an AccessGroupV1 named group-1 and prints its YAML representation.
  aclient get accessgroups group-1 -o yaml

  # Loads all AccessGroupV1 and prints their default representation.
  aclient get accessgroups
  
  # Loads all ExternalUsers and prints their default representation.
  aclient get externalusers


  # Loads all ExternalUsers from source "source1" and prints their default representation.
  aclient get externalusers source1

  # Loads an ExternalUser from source "source1" named "user1" and prints its default representation.
  aclient get externalusers source1 user1`,
	Run: func(cmd *cobra.Command, args []string) {
		// Parse Args.
		getArgs, err := parseGetArgs(args)
		if err != nil {
			cmd.PrintErrln(err)

			// Fatal error.
			os.Exit(-1)
		}

		if err := get.Get(apiClient, getCmdFlags.outputFormat, cmd.OutOrStdout(), getArgs.kind, getArgs.source, getArgs.names...); err != nil {
			cmd.PrintErrf("get#Get error: %s", err)

			os.Exit(-1)
		}
	},
	ValidArgs: supportedResources,
	Args: func(cmd *cobra.Command, args []string) error {
		// Args count.
		// Must be at least one arg: kind and optionally the second arg - the resource name.
		if len(args) == 0 {
			return fmt.Errorf("%w: requires at least 1 arg (kind), only received %d", ErrGetArgs, len(args))
		}

		// First arg must be in supported kinds.
		var found bool
		for i := range supportedResources {
			if supportedResources[i] == args[0] {
				found = true

				break
			}
		}

		if !found {
			return fmt.Errorf("%w: %q is not a supported resource kind", ErrGetArgs, args[0])
		}

		// Output Format.
		if err := printer.ValidateOutputFormat(getCmdFlags.outputFormat); err != nil {
			return err
		}

		return nil
	},
}

func init() {
	rootCmd.AddCommand(getCmd)

	getCmd.PersistentFlags().StringVarP(
		&getCmdFlags.outputFormat, "output", "o", "",
		"output format for the create result. Supported values are: json, yaml.")
}

// parseGetArgs parses arguments and stores them in global variable getCmdArgs.
func parseGetArgs(args []string) (*getCmdArgsT, error) {
	var (
		argsInd = 0
		getArgs getCmdArgsT
	)

	// 1 is a minimum args count (kind only).
	// The "true" situation must not be possible here because args validator must catch
	// the 0 args situation earlier.
	if len(args) < 1 {
		return nil, fmt.Errorf("%w: requires at least 1 arg (kind), only received %d", ErrGetArgs, len(args))
	}

	// Map cli resource to storage.ResourceKindT.
	kind, ok := supportedResourcesToStorageResources[args[argsInd]]
	if !ok {
		return nil, fmt.Errorf("%w: supportedResourcesToStorageResources does not contain a map from %q", ErrGetArgs, args[argsInd])
	}

	getArgs.kind = kind

	argsInd++

	// Two and more arguments are required in case of resources which have "source".
	if kind == storage.ExternalUserSessionKind || kind == storage.ExternalGroupKind || kind == storage.ExternalUserKind {
		// 2 is the minimum args length here because the first arg is the Kind and the second must be the Source.
		if len(args) < 2 { // nolint:gomnd
			return nil, fmt.Errorf("%w: given resource Kind %q required a Source which was not given", ErrGetArgs, kind)
		}

		// Resources which have source.
		getArgs.source = storage.ResourceNameT(args[argsInd])

		argsInd++
	}

	// One Arg - only a Kind.
	if len(args) == 1 {
		return &getArgs, nil
	}

	// Resources which do not have "source".
	// Assume arguments are names.
	// Further arguments are assumed the resource kind names.
	names := make([]storage.ResourceNameT, 0, len(args)-argsInd)

	for _, n := range args[argsInd:] {
		names = append(names, storage.ResourceNameT(n))
	}

	getArgs.names = names

	return &getArgs, nil
}
