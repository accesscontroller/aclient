/*
Copyright © 2020 Dmitrii Ermakov <demonihin@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
package cmd

import (
	"errors"
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
	"gitlab.com/accesscontroller/aclient/pkg/delete"
	"gitlab.com/accesscontroller/aclient/pkg/printer"
)

// minDeleteCmdArgsLen -  2 is a minimum which means delete <kind> and <names>.
const minDeleteCmdArgsLen = 2

var ErrDeleteArgs = errors.New("invalid delete command arguments")

// deleteCmdFlags delete command configuration.
var deleteCmdFlags struct {
	outputFormat string

	inputFiles []string
}

// deleteCmdArgsT contains parsed delete command arguments.
type deleteCmdArgsT struct {
	kind   storage.ResourceKindT
	source storage.ResourceNameT
	names  []storage.ResourceNameT
}

// deleteCmd represents the delete command.
var deleteCmd = &cobra.Command{
	Aliases: []string{"d"},
	Use:     "delete < <kind> [source] <names, separated by space> | -f <files with the resources> >",
	Short:   "deletes resources",
	Long: `Call the command with resources kind, source and names to delete resources.
The command could be called with -f flag to provide a JSON or YAML file with resources to delete.
The command prints the last state of the deleted resources. The format could be changed using -o flag.

` + supportedResourcesFormatted,
	Example: `  # Deletes an AccessGroupV1 named group-1.
  aclient delete accessgroups group-1

  # Deletes all AccessGroupV1 which are in the given file.
  aclient delete accessgroups -f resources.yaml`,
	ValidArgs: supportedResources,
	Args: func(cmd *cobra.Command, args []string) error {
		// Args count.
		// Must be at least one arg: kind and optionally the second arg - the resource name.
		// Or flag "-f" set.
		if len(args) == 0 && len(deleteCmdFlags.inputFiles) == 0 {
			return fmt.Errorf("%w: requires at least 1 arg (kind), only received %d and -f flag is not set",
				ErrDeleteArgs, len(args))
		}

		// Check the resources Kind.
		if len(deleteCmdFlags.inputFiles) == 0 {
			// First arg must be in supported kinds.
			var found bool
			for i := range supportedResources {
				if supportedResources[i] == args[0] {
					found = true

					break
				}
			}

			if !found {
				return fmt.Errorf("%w: %q is not a supported resource kind", ErrDeleteArgs, args[0])
			}
		}

		// Output Format.
		if err := printer.ValidateOutputFormat(deleteCmdFlags.outputFormat); err != nil {
			return err
		}

		return nil
	},
	Run: func(cmd *cobra.Command, args []string) {
		// If files defined - use them.
		if len(deleteCmdFlags.inputFiles) > 0 {
			if err := delete.FromFiles(apiClient,
				deleteCmdFlags.outputFormat, cmd.OutOrStdout(),
				deleteCmdFlags.inputFiles...); err != nil {
				cmd.PrintErrf("delete#FromFiles error: %s", err)

				os.Exit(-1)
			}

			return
		}

		// Delete from the resource kind, source, names.
		if len(args) < minDeleteCmdArgsLen {
			cmd.PrintErr("Not enough arguments")

			_ = cmd.Usage()

			os.Exit(-1)
		}

		// Call Delete FromNames.
		// Parse Args.
		delArgs, err := parseDeleteArgs(args)
		if err != nil {
			cmd.PrintErrln(err)

			// Fatal error.
			os.Exit(-1)
		}

		if err := delete.FromNames(apiClient, deleteCmdFlags.outputFormat, cmd.OutOrStdout(),
			delArgs.kind, delArgs.source, delArgs.names...); err != nil {
			cmd.PrintErrf("delete#FromNames error: %s", err)

			os.Exit(-1)
		}
	},
}

func init() {
	rootCmd.AddCommand(deleteCmd)

	deleteCmd.Flags().StringArrayVarP(&deleteCmdFlags.inputFiles, "filenames", "f", nil,
		"input files to delete resources (required).")
	deleteCmd.Flags().StringVarP(
		&deleteCmdFlags.outputFormat, "output", "o", "",
		"output format for the delete result. Supported values are: json, yaml.")
}

// parseDeleteArgs parses arguments and stores them in global variable deleteCmdArgs.
func parseDeleteArgs(args []string) (*deleteCmdArgsT, error) {
	var (
		argsInd    = 0
		deleteArgs deleteCmdArgsT
	)

	// 2 is a minimum args count (kind and name only).
	if len(args) < minDeleteCmdArgsLen {
		return nil, fmt.Errorf("%w: requires at least 2 args (kind, name), only received %d", ErrGetArgs, len(args))
	}

	// Map cli resource to storage.ResourceKindT.
	kind, ok := supportedResourcesToStorageResources[args[argsInd]]
	if !ok {
		return nil, fmt.Errorf("%w: supportedResourcesToStorageResources does not contain a map from %q", ErrGetArgs, args[argsInd])
	}

	deleteArgs.kind = kind

	argsInd++

	// Three and more arguments are required in case of resources which have "source".
	if kind == storage.ExternalUserSessionKind || kind == storage.ExternalGroupKind || kind == storage.ExternalUserKind {
		// 3 is the minimum args length here because the first arg is the Kind and the second must be the Source.
		if len(args) < 3 { // nolint:gomnd
			return nil, fmt.Errorf("%w: given resource Kind %q required a Source which was not given", ErrGetArgs, kind)
		}

		// Resources which have source.
		deleteArgs.source = storage.ResourceNameT(args[argsInd])

		argsInd++
	}

	// Resources which do not have "source".
	// Assume arguments are names.
	// Further arguments are assumed the resource kind names.
	names := make([]storage.ResourceNameT, 0, len(args)-argsInd)

	for _, n := range args[argsInd:] {
		names = append(names, storage.ResourceNameT(n))
	}

	deleteArgs.names = names

	return &deleteArgs, nil
}
