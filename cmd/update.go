/*
Copyright © 2020 Dmitrii Ermakov <demonihin@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
package cmd

import (
	"errors"
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
	"gitlab.com/accesscontroller/aclient/pkg/printer"
	"gitlab.com/accesscontroller/aclient/pkg/update"
)

const maxExpectedUpdateArgsLen = 1 // 1 argument - one resource + rename. Zero - many resources, no rename.

var ErrUpdateArgs = errors.New("invalid update command arguments")

// updateCmdFlags update command configuration.
var updateCmdFlags struct {
	outputFormat string

	inputFiles []string
}

// updateCmd represents the update command.
var updateCmd = &cobra.Command{
	Aliases: []string{"u"},
	Use:     "update [name] -f <patchfile>",
	Short:   "Updates an existing resource",
	Long: `Call the command with a resource name (optional) and a patch to update the resource.
If the command is called with a "name" flag than the patchfile must contain
only one resource and the resource will be patched and renamed.
The "name" flag is the current (before update) the resource's name.

` + supportedResourcesFormatted,
	Example: `  # Updates an AccessGroupV1 named group-1.
  aclient update group-1 -f updated-group.yaml
  
  # Updates an AccessGroupV1 named group-1 and prints its JSON representation.
  aclient update group-1 -f updated-group.yaml -o json

  # Updates an AccessGroupV1 named group-1 and prints its JSON representation.
  aclient update group-1 -f updated-group.json -o yaml`,
	Args: func(cmd *cobra.Command, args []string) error {
		// Check that there is at most one argument (a resource name to rename).
		if len(args) > maxExpectedUpdateArgsLen {
			return fmt.Errorf("%w: must be either 0 or 1 argument", ErrUpdateArgs)
		}

		// Output Format.
		if err := printer.ValidateOutputFormat(updateCmdFlags.outputFormat); err != nil {
			return err
		}

		return nil
	},
	Run: func(cmd *cobra.Command, args []string) {
		// One resource + Rename.
		if len(args) != 0 {
			oldName := storage.ResourceNameT(args[0])

			if err := update.WithRename(apiClient, updateCmdFlags.outputFormat, cmd.OutOrStdout(),
				oldName, updateCmdFlags.inputFiles[0]); err != nil {
				cmd.PrintErrf("update#WithRename error %s", err)

				os.Exit(-1)
			}
		}

		// Multiple resources from files.
		if err := update.WithoutRename(apiClient, updateCmdFlags.outputFormat, cmd.OutOrStdout(),
			updateCmdFlags.inputFiles...); err != nil {
			cmd.PrintErrf("update#WithoutRename error %s", err)

			os.Exit(-1)
		}
	},
}

func init() {
	rootCmd.AddCommand(updateCmd)

	updateCmd.Flags().StringVarP(
		&updateCmdFlags.outputFormat, "output", "o", "",
		"output format for the create result. Supported values are: json, yaml.")
	updateCmd.Flags().StringArrayVarP(&updateCmdFlags.inputFiles, "filenames", "f", nil,
		"input files to update resources (required).")

	if err := updateCmd.MarkFlagRequired("filenames"); err != nil {
		panic(err)
	}
}
