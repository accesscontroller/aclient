/*
Copyright © 2020 Dmitrii Ermakov <demonihin@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
package cmd

import (
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/accesscontroller/aclient/pkg/create"
	"gitlab.com/accesscontroller/aclient/pkg/printer"
)

// createCmdFlags represents the create command run flags.
var createCmdFlags struct {
	outputFormat string

	inputFiles []string
}

// createCmd represents the create command.
var createCmd = &cobra.Command{
	Aliases: []string{"c"},
	Use:     "create -f <resources file>",
	Short:   "Creates resources from given file.",
	Long: `To create resources call the command and provide a file with a payload:
The files could be either JSON or YAML formatted.
  -o, --output <format> flag makes the command print the created resources using JSON or YAML format.

` + supportedResourcesFormatted,
	Example: `  # Creates all resources defined in accessgroups.yaml in YAML format.
  aclient create -f accessgroups.yaml

  # Creates  all resources defined in accessgroups.yaml in JSON format.
  aclient create -f accessgroups.json

  # Creates  all resources defined in accessgroups.yaml in JSON format and prints created resources in YAML format.
  aclient create -f accessgroups.json -o yaml`,
	Args: func(cmd *cobra.Command, args []string) error {
		// Output Format.
		if err := printer.ValidateOutputFormat(createCmdFlags.outputFormat); err != nil {
			return err
		}

		return nil
	},
	Run: func(cmd *cobra.Command, args []string) {
		if err := create.Create(apiClient, createCmdFlags.outputFormat, cmd.OutOrStdout(), createCmdFlags.inputFiles...); err != nil {
			cmd.PrintErrf("create#Create error: %s", err)

			os.Exit(1)
		}
	},
}

func init() {
	rootCmd.AddCommand(createCmd)

	// Flags.
	createCmd.Flags().StringVarP(
		&createCmdFlags.outputFormat, "output", "o", "",
		"output format for the create result. Supported values are: json, yaml.")
	createCmd.Flags().StringArrayVarP(&createCmdFlags.inputFiles, "filenames", "f", nil,
		"input files to create resources.")

	if err := createCmd.MarkFlagRequired("filenames"); err != nil {
		panic(err)
	}
}
