OUTPUT_DIR := "out"
BINARY_NAME := "aclient"

help:
	@echo "Hello! I'm not you help. Read the Makefile, please."

output-dir:
	mkdir -p ${OUTPUT_DIR}/

clean:
	rm -rf ${OUTPUT_DIR}/

copy-config: output-dir
	cp .aclient.yaml ${OUTPUT_DIR}/

build: output-dir copy-config
	go build -o ${OUTPUT_DIR}/${BINARY_NAME}