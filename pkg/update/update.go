package update

import (
	"errors"
	"fmt"
	"io"

	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
	"gitlab.com/accesscontroller/aclient/pkg/filereader"
	"gitlab.com/accesscontroller/aclient/pkg/printer"
	"gitlab.com/accesscontroller/cclient.v1"
)

var ErrUpdateInput = errors.New("incorrect input")

func readResources(inputFiles ...string) ([]interface{}, error) {
	// Read input.
	resources, err := filereader.Read(inputFiles...)
	if err != nil {
		return nil, err
	}

	// Make resources for cclient.
	var reqRess = make([]interface{}, len(resources))

	for i, v := range resources {
		reqRess[i] = v.Value
	}

	return reqRess, nil
}

// WithRename updates a resource from a file using given "Old" resource name
// to lookup the resource.
func WithRename(cl *cclient.CClient, outputFormat string, out io.Writer,
	oldName storage.ResourceNameT, inputFile string) error {
	// Read.
	resources, err := readResources(inputFile)
	if err != nil {
		return err
	}

	// Check that only 1 resource provided.
	if len(resources) > 1 {
		return fmt.Errorf("%w: WithRename call must be provided with only one resource for update", ErrUpdateInput)
	}

	// Update.
	// Update.
	updated, err := cl.UpdateResourcesV1(true, resources...)
	if err != nil {
		return err
	}

	// Print.
	if _, err := printer.Write(outputFormat, out, updated...); err != nil {
		return err
	}

	return nil
}

// WithoutRename updates resources from files without rename.
func WithoutRename(cl *cclient.CClient, outputFormat string, out io.Writer, inputFiles ...string) error {
	// Read.
	resources, err := readResources(inputFiles...)
	if err != nil {
		return err
	}

	// Update.
	updated, err := cl.UpdateResourcesV1(true, resources...)
	if err != nil {
		return err
	}

	// Print.
	if _, err := printer.Write(outputFormat, out, updated...); err != nil {
		return err
	}

	return nil
}
