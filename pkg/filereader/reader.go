package filereader

import (
	"fmt"
	"os"

	"gitlab.com/accesscontroller/aclient/pkg/info"
	"gitlab.com/accesscontroller/aclient/pkg/serializer"
)

// Read reads all resources from provided files.
func Read(files ...string) ([]*info.Resource, error) {
	var (
		resources = make([]*info.Resource, 0)
	)

	for _, fn := range files {
		r, err := readFile(fn)
		if err != nil {
			return nil, err
		}

		resources = append(resources, r...)
	}

	return resources, nil
}

func readFile(file string) ([]*info.Resource, error) {
	f, err := os.Open(file)
	if err != nil {
		return nil, fmt.Errorf("can not open file %q: %w", file, err)
	}

	defer f.Close()

	return serializer.Decode(f)
}
