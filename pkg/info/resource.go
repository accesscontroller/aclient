package info

import "gitlab.com/accesscontroller/accesscontroller/controller/storage"

// Resource contains one Resource as interface{} and its kind.
// Value is a pointer to actual data.
type Resource struct {
	Kind storage.ResourceKindT

	Value interface{}
}
