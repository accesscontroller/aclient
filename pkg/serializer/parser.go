package serializer

import (
	"fmt"
	"io"
	"io/ioutil"

	"gitlab.com/accesscontroller/aclient/pkg/info"
)

// Decode reads input and decodes it as storage resources.
// Supported formats are: JSON Object, JSON Array, YAML Documents [separated by ---].
// []interface{} contains pointers to the resources.
func Decode(r io.Reader) ([]*info.Resource, error) {
	// Read All.
	data, err := ioutil.ReadAll(r)
	if err != nil {
		return nil, fmt.Errorf("can not read data from input: %w", err)
	}

	return unmarshal(data)
}

// Unmarshal decodes input as storage resources.
// Supported formats are: JSON Object, JSON Array, YAML Documents [separated by ---].
// []interface{} contains pointers to the resources.
func Unmarshal(data []byte) ([]*info.Resource, error) {
	return unmarshal(data)
}
