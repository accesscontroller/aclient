package serializer

import (
	"testing"

	"github.com/stretchr/testify/suite"
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
	"gitlab.com/accesscontroller/aclient/pkg/info"
)

type TestUnmarshalSuite struct {
	suite.Suite
}

func (ts *TestUnmarshalSuite) TestUnmarshalYAML() {
	var tests = []*struct {
		name string
		in   []byte

		expectedInfos []*info.Resource

		expectedError error
	}{
		{
			name: "Unmarshal AccessGroupV1s success",
			in: []byte(`
---
metadata:
  apiVersion: v1
  eTag: 1
  externalResource: false
  kind: AccessGroup
  name: access group 1
data:
  defaultPolicy: allow
  extraAccessDenyMessage: access denied
  orderInAccessRulesList: 10
  totalGroupSpeedLimitBps: 100000
  userGroupSpeedLimitBps: 1000
---
metadata:
  apiVersion: v1
  eTag: 2
  externalResource: false
  kind: AccessGroup
  name: access group 2
data:
  defaultPolicy: deny
  extraAccessDenyMessage: access denied
  orderInAccessRulesList: 11
  totalGroupSpeedLimitBps: 200000
  userGroupSpeedLimitBps: 2000
`,
			),
			expectedInfos: []*info.Resource{
				{
					Kind: storage.AccessGroupKind,
					Value: &storage.AccessGroupV1{
						Metadata: storage.MetadataV1{
							APIVersion:       storage.APIVersionV1Value,
							ETag:             1,
							ExternalResource: false,
							ExternalSource:   nil,
							Kind:             storage.AccessGroupKind,
							Name:             "access group 1",
						},
						Data: storage.AccessGroupDataV1{
							DefaultPolicy:           storage.DefaultAccessPolicyAllow,
							ExtraAccessDenyMessage:  "access denied",
							OrderInAccessRulesList:  10,
							TotalGroupSpeedLimitBps: 100_000,
							UserGroupSpeedLimitBps:  1_000,
						},
					},
				},
				{
					Kind: storage.AccessGroupKind,
					Value: &storage.AccessGroupV1{
						Metadata: storage.MetadataV1{
							APIVersion:       storage.APIVersionV1Value,
							ETag:             2,
							ExternalResource: false,
							ExternalSource:   nil,
							Kind:             storage.AccessGroupKind,
							Name:             "access group 2",
						},
						Data: storage.AccessGroupDataV1{
							DefaultPolicy:           storage.DefaultAccessPolicyDeny,
							ExtraAccessDenyMessage:  "access denied",
							OrderInAccessRulesList:  11,
							TotalGroupSpeedLimitBps: 200_000,
							UserGroupSpeedLimitBps:  2_000,
						},
					},
				},
			},
		},
	}

	for _, test := range tests {
		gotInfos, gotErr := unmarshal(test.in)

		if !ts.Equalf(test.expectedError, gotErr, "Failed test %q", test.name) {
			ts.FailNowf("Aborted", "test %q", test.name)
		}

		ts.Equalf(test.expectedInfos, gotInfos, "test %q", test.name)
	}
}

func (ts *TestUnmarshalSuite) TestUnmarshalJSONObject() {
	var tests = []*struct {
		name string
		in   []byte

		expectedInfos []*info.Resource

		expectedError error
	}{
		{
			name: "Unmarshal AccessGroupV1s success",
			in: []byte(`
{
	"metadata": {
		"apiVersion": "v1",
		"eTag": 1,
		"externalResource": false,
		"kind": "AccessGroup",
		"name": "access group 1"
	},
	"data": {
		"defaultPolicy": "allow",
		"extraAccessDenyMessage": "access denied",
		"orderInAccessRulesList": 10,
		"totalGroupSpeedLimitBps": 100000,
		"userGroupSpeedLimitBps": 1000
	}
}
`,
			),
			expectedInfos: []*info.Resource{
				{
					Kind: storage.AccessGroupKind,
					Value: &storage.AccessGroupV1{
						Metadata: storage.MetadataV1{
							APIVersion:       storage.APIVersionV1Value,
							ETag:             1,
							ExternalResource: false,
							ExternalSource:   nil,
							Kind:             storage.AccessGroupKind,
							Name:             "access group 1",
						},
						Data: storage.AccessGroupDataV1{
							DefaultPolicy:           storage.DefaultAccessPolicyAllow,
							ExtraAccessDenyMessage:  "access denied",
							OrderInAccessRulesList:  10,
							TotalGroupSpeedLimitBps: 100_000,
							UserGroupSpeedLimitBps:  1_000,
						},
					},
				},
			},
		},
	}

	for _, test := range tests {
		gotInfos, gotErr := unmarshal(test.in)

		if !ts.Equalf(test.expectedError, gotErr, "Failed test %q", test.name) {
			ts.FailNowf("Aborted", "test %q", test.name)
		}

		ts.Equalf(test.expectedInfos, gotInfos, "test %q", test.name)
	}
}

func (ts *TestUnmarshalSuite) TestUnmarshalJSONArray() {
	var tests = []*struct {
		name string
		in   []byte

		expectedInfos []*info.Resource

		expectedError error
	}{
		{
			name: "Unmarshal AccessGroupV1s success",
			in: []byte(`
[
	{
		"metadata": {
			"apiVersion": "v1",
			"eTag": 1,
			"externalResource": false,
			"kind": "AccessGroup",
			"name": "access group 1"
		},
		"data": {
			"defaultPolicy": "allow",
			"extraAccessDenyMessage": "access denied",
			"orderInAccessRulesList": 10,
			"totalGroupSpeedLimitBps": 100000,
			"userGroupSpeedLimitBps": 1000
		}
	},
	{
		"metadata": {
			"apiVersion": "v1",
			"eTag": 2,
			"externalResource": false,
			"kind": "AccessGroup",
			"name": "access group 2"
		},
		"data": {
			"defaultPolicy": "deny",
			"extraAccessDenyMessage": "access denied 2",
			"orderInAccessRulesList": 11,
			"totalGroupSpeedLimitBps": 100001,
			"userGroupSpeedLimitBps": 1001
		}
	}
]
`,
			),
			expectedInfos: []*info.Resource{
				{
					Kind: storage.AccessGroupKind,
					Value: &storage.AccessGroupV1{
						Metadata: storage.MetadataV1{
							APIVersion:       storage.APIVersionV1Value,
							ETag:             1,
							ExternalResource: false,
							ExternalSource:   nil,
							Kind:             storage.AccessGroupKind,
							Name:             "access group 1",
						},
						Data: storage.AccessGroupDataV1{
							DefaultPolicy:           storage.DefaultAccessPolicyAllow,
							ExtraAccessDenyMessage:  "access denied",
							OrderInAccessRulesList:  10,
							TotalGroupSpeedLimitBps: 100_000,
							UserGroupSpeedLimitBps:  1_000,
						},
					},
				},
				{
					Kind: storage.AccessGroupKind,
					Value: &storage.AccessGroupV1{
						Metadata: storage.MetadataV1{
							APIVersion:       storage.APIVersionV1Value,
							ETag:             2,
							ExternalResource: false,
							ExternalSource:   nil,
							Kind:             storage.AccessGroupKind,
							Name:             "access group 2",
						},
						Data: storage.AccessGroupDataV1{
							DefaultPolicy:           storage.DefaultAccessPolicyDeny,
							ExtraAccessDenyMessage:  "access denied 2",
							OrderInAccessRulesList:  11,
							TotalGroupSpeedLimitBps: 100_001,
							UserGroupSpeedLimitBps:  1_001,
						},
					},
				},
			},
		},
	}

	for _, test := range tests {
		gotInfos, gotErr := unmarshal(test.in)

		if !ts.Equalf(test.expectedError, gotErr, "Failed test %q", test.name) {
			ts.FailNowf("Aborted", "test %q", test.name)
		}

		ts.Equalf(test.expectedInfos, gotInfos, "test %q", test.name)
	}
}

func TestUnmarshal(t *testing.T) {
	suite.Run(t, &TestUnmarshalSuite{})
}
