package serializer

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"unicode"

	"gopkg.in/yaml.v3"

	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
	"gitlab.com/accesscontroller/aclient/pkg/info"
)

var (
	ErrUnsupportedKind = errors.New("unsupported resource Kind")
	ErrDetectInputType = errors.New("can not detect input type")
)

type inputType int

const (
	jsonObject inputType = 1
	jsonArray  inputType = 2
	yamlDocs   inputType = 3
)

func detectType(bts []byte) (inputType, error) {
	// Check first printable character.
	s := string(bts)

	for _, c := range s {
		if !unicode.IsGraphic(c) {
			continue
		}

		switch c {
		case '[':
			return jsonArray, nil
		case '{':
			return jsonObject, nil
		default:
			return yamlDocs, nil
		}
	}

	return 0, ErrDetectInputType
}

func unmarshalJSONObject(bts []byte) (*info.Resource, error) {
	// Metadata.
	var md mdWrapper

	if err := json.Unmarshal(bts, &md); err != nil {
		return nil, err
	}

	// Resource.
	var result interface{}

	switch md.Metadata.Kind {
	case storage.AccessGroup2ExternalGroupListV1RelationsKind:
		result = &storage.AccessGroup2ExternalGroupListRelationsV1{}
	case storage.AccessGroup2WebResourceCategoryListV1RelationsKind:
		result = &storage.AccessGroup2WebResourceCategoryListRelationsV1{}
	case storage.AccessGroupKind:
		result = &storage.AccessGroupV1{}
	case storage.ExternalGroup2ExternalUserListV1RelationsKind:
		result = &storage.ExternalGroup2ExternalUserListRelationsV1{}
	case storage.ExternalGroupKind:
		result = &storage.ExternalGroupV1{}
	case storage.ExternalSessionsSourceKind:
		result = &storage.ExternalSessionsSourceV1{}
	case storage.ExternalUser2ExternalGroupListV1RelationsKind:
		result = &storage.ExternalUser2ExternalGroupListRelationsV1{}
	case storage.ExternalUserSessionKind:
		result = &storage.ExternalUserSessionV1{}
	case storage.ExternalUsersGroupsSourceKind:
		result = &storage.ExternalUsersGroupsSourceV1{}
	case storage.ExternalUserKind:
		result = &storage.ExternalUserV1{}
	case storage.WebResourceCategoryKind:
		result = &storage.WebResourceCategoryV1{}
	case storage.WebResourceKind:
		result = &storage.WebResourceV1{}
	default:
		return nil, fmt.Errorf("%w: Kind %q", ErrUnsupportedKind, md.Metadata.Kind)
	}

	// Unmarshal.
	if err := json.Unmarshal(bts, result); err != nil {
		return nil, err
	}

	return &info.Resource{
		Kind:  md.Metadata.Kind,
		Value: result,
	}, nil
}

func prepareStorage(mds []mdWrapper) ([]storage.ResourceKindT, []interface{}, error) {
	resources := make([]interface{}, 0, len(mds))
	kinds := make([]storage.ResourceKindT, 0, len(mds))

	for i := range mds {
		switch mds[i].Metadata.Kind {
		case storage.AccessGroup2ExternalGroupListV1RelationsKind:
			resources = append(resources, &storage.AccessGroup2ExternalGroupListRelationsV1{})
		case storage.AccessGroup2WebResourceCategoryListV1RelationsKind:
			resources = append(resources, &storage.AccessGroup2WebResourceCategoryListRelationsV1{})
		case storage.AccessGroupKind:
			resources = append(resources, &storage.AccessGroupV1{})
		case storage.ExternalGroup2ExternalUserListV1RelationsKind:
			resources = append(resources, &storage.ExternalGroup2ExternalUserListRelationsV1{})
		case storage.ExternalGroupKind:
			resources = append(resources, &storage.ExternalGroupV1{})
		case storage.ExternalSessionsSourceKind:
			resources = append(resources, &storage.ExternalSessionsSourceV1{})
		case storage.ExternalUser2ExternalGroupListV1RelationsKind:
			resources = append(resources, &storage.ExternalUser2ExternalGroupListRelationsV1{})
		case storage.ExternalUserSessionKind:
			resources = append(resources, &storage.ExternalUserSessionV1{})
		case storage.ExternalUsersGroupsSourceKind:
			resources = append(resources, &storage.ExternalUsersGroupsSourceV1{})
		case storage.ExternalUserKind:
			resources = append(resources, &storage.ExternalUserV1{})
		case storage.WebResourceCategoryKind:
			resources = append(resources, &storage.WebResourceCategoryV1{})
		case storage.WebResourceKind:
			resources = append(resources, &storage.WebResourceV1{})
		default:
			return nil, nil, fmt.Errorf("%w: Kind %q", ErrUnsupportedKind, mds[i].Metadata.Kind)
		}

		kinds = append(kinds, mds[i].Metadata.Kind)
	}

	return kinds, resources, nil
}

func unmarshalJSONArray(bts []byte) ([]*info.Resource, error) {
	// Unmarshal Metadata.
	var mds []mdWrapper

	if err := json.Unmarshal(bts, &mds); err != nil { //nolint:shadow
		return nil, fmt.Errorf("can not Unmarshal MetadataV1s: %w", err)
	}

	// For every resource unmarshal resource.

	// Prepare storage.
	kinds, resources, err := prepareStorage(mds)
	if err != nil {
		return nil, err
	}

	// Unmarshal.
	if err := json.Unmarshal(bts, &resources); err != nil {
		return nil, fmt.Errorf("can not Unmarshal Resources: %w", err)
	}

	// Make result.
	result := make([]*info.Resource, len(resources))
	for i, k := range kinds {
		result[i] = &info.Resource{
			Kind:  k,
			Value: resources[i],
		}
	}

	return result, nil
}

func unmarshalYAMLDocs(bts []byte) ([]*info.Resource, error) {
	// Unmarshal YAML documents as Metadata.
	var mds []mdWrapper

	MDDec := yaml.NewDecoder(bytes.NewReader(bts))

	for {
		var md mdWrapper

		if err := MDDec.Decode(&md); err != nil { //nolint:shadow
			if err == io.EOF {
				break
			}

			return nil, fmt.Errorf("can not Unmarshal MetadataV1s: %w", err)
		}

		mds = append(mds, md)
	}

	// For every resource unmarshal resource.

	// Prepare storage.
	kinds, resources, err := prepareStorage(mds)
	if err != nil {
		return nil, err
	}

	// Unmarshal.
	ResDec := yaml.NewDecoder(bytes.NewReader(bts))

	for i := 0; i < len(resources); i++ {
		if err := ResDec.Decode(resources[i]); err != nil {
			return nil, fmt.Errorf("can not Unmarshal Resources: %w", err)
		}
	}

	// Make result.
	result := make([]*info.Resource, len(resources))
	for i, k := range kinds {
		result[i] = &info.Resource{
			Kind:  k,
			Value: resources[i],
		}
	}

	return result, nil
}

func unmarshal(bts []byte) ([]*info.Resource, error) {
	// Check the file type.
	inType, err := detectType(bts)
	if err != nil {
		return nil, err
	}

	switch inType {
	case jsonObject:
		res, err := unmarshalJSONObject(bts) // nolint: shadow

		return []*info.Resource{res}, err
	case jsonArray:
		return unmarshalJSONArray(bts)
	case yamlDocs:
		return unmarshalYAMLDocs(bts)
	default:
		return nil, ErrDetectInputType
	}
}
