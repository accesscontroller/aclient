package serializer

import (
	"encoding/json"
	"fmt"
	"io"
	"text/tabwriter"

	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
	"gitlab.com/accesscontroller/aclient/pkg/info"
	"gopkg.in/yaml.v3"
)

// YAMLIndent defines YAML files indentation.
const YAMLIndent = 2

// EncodeYAML stores given resources as YAML documents separated by ---.
func EncodeYAML(w io.Writer, v ...interface{}) error {
	enc := yaml.NewEncoder(w)

	enc.SetIndent(YAMLIndent)

	for i := range v {
		if err := enc.Encode(v[i]); err != nil {
			return fmt.Errorf("can not EncodeYAML: %w", err)
		}
	}

	return nil
}

// EncodeJSON stores given resources as JSON Object(one resource) or JSON Array (many resources).
func EncodeJSON(w io.Writer, v ...interface{}) error {
	enc := json.NewEncoder(w)

	if len(v) == 1 { // 1 means - a one JSON Object.
		return enc.Encode(v[0])
	}

	// Store as Array.
	return enc.Encode(v)
}

// encodeMetadataV1Brief encodes given MetadataV1.
func encodeMetadataV1Brief(w io.Writer, md *storage.MetadataV1) error {
	if md.ExternalSource == nil {
		_, wrErr := fmt.Fprintln(w, md.Kind, "\t", md.Name)

		return wrErr
	}

	_, wrErr := fmt.Fprintln(w, md.Kind, "\t", md.Name, "@", md.ExternalSource.SourceName)

	return wrErr
}

// BriefEncoder a type could implement the resource to customize EncodeBrief behavior.
type BriefEncoder interface {
	EncodeBrief(io.Writer) error
}

// encodeResourceV1sBrief encodes given resources using "brief" printing.
// Unwraps info.Resource and info.Resource slices.
func encodeResourceV1sBrief(wrt io.Writer, v ...interface{}) error { // nolint:funlen,gocyclo
	for i := range v {
		var wrErr error

		switch tpd := v[i].(type) {
		// Handle customized BriefEncoder.
		case BriefEncoder:
			wrErr = tpd.EncodeBrief(wrt)

		case []interface{}: // Handle slice case.
			wrErr = encodeResourceV1sBrief(wrt, tpd...)

		case info.Resource:
			wrErr = encodeResourceV1sBrief(wrt, tpd.Value)

		case *info.Resource:
			wrErr = encodeResourceV1sBrief(wrt, tpd.Value)

		case []info.Resource:
			for i := range tpd {
				wrErr = encodeResourceV1sBrief(wrt, tpd[i].Value)
			}

		case []*info.Resource:
			for i := range tpd {
				wrErr = encodeResourceV1sBrief(wrt, tpd[i].Value)
			}

		case storage.AccessGroup2ExternalGroupListRelationsV1:
			wrErr = encodeMetadataV1Brief(wrt, &tpd.Metadata)

		case *storage.AccessGroup2ExternalGroupListRelationsV1:
			wrErr = encodeMetadataV1Brief(wrt, &tpd.Metadata)

		case storage.AccessGroup2WebResourceCategoryListRelationsV1:
			wrErr = encodeMetadataV1Brief(wrt, &tpd.Metadata)

		case *storage.AccessGroup2WebResourceCategoryListRelationsV1:
			wrErr = encodeMetadataV1Brief(wrt, &tpd.Metadata)

		case storage.AccessGroupV1:
			wrErr = encodeMetadataV1Brief(wrt, &tpd.Metadata)

		case *storage.AccessGroupV1:
			wrErr = encodeMetadataV1Brief(wrt, &tpd.Metadata)

		case storage.ExternalGroup2ExternalUserListRelationsV1:
			wrErr = encodeMetadataV1Brief(wrt, &tpd.Metadata)

		case *storage.ExternalGroup2ExternalUserListRelationsV1:
			wrErr = encodeMetadataV1Brief(wrt, &tpd.Metadata)

		case storage.ExternalUser2ExternalGroupListRelationsV1:
			wrErr = encodeMetadataV1Brief(wrt, &tpd.Metadata)

		case *storage.ExternalUser2ExternalGroupListRelationsV1:
			wrErr = encodeMetadataV1Brief(wrt, &tpd.Metadata)

		case storage.ExternalGroupV1:
			wrErr = encodeMetadataV1Brief(wrt, &tpd.Metadata)

		case *storage.ExternalGroupV1:
			wrErr = encodeMetadataV1Brief(wrt, &tpd.Metadata)

		case storage.ExternalSessionsSourceV1:
			wrErr = encodeMetadataV1Brief(wrt, &tpd.Metadata)

		case *storage.ExternalSessionsSourceV1:
			wrErr = encodeMetadataV1Brief(wrt, &tpd.Metadata)

		case storage.ExternalUserSessionV1:
			wrErr = encodeMetadataV1Brief(wrt, &tpd.Metadata)

		case *storage.ExternalUserSessionV1:
			wrErr = encodeMetadataV1Brief(wrt, &tpd.Metadata)

		case storage.ExternalUsersGroupsSourceV1:
			wrErr = encodeMetadataV1Brief(wrt, &tpd.Metadata)

		case *storage.ExternalUsersGroupsSourceV1:
			wrErr = encodeMetadataV1Brief(wrt, &tpd.Metadata)

		case storage.ExternalUserV1:
			wrErr = encodeMetadataV1Brief(wrt, &tpd.Metadata)

		case *storage.ExternalUserV1:
			wrErr = encodeMetadataV1Brief(wrt, &tpd.Metadata)

		case storage.WebResourceCategoryV1:
			wrErr = encodeMetadataV1Brief(wrt, &tpd.Metadata)

		case *storage.WebResourceCategoryV1:
			wrErr = encodeMetadataV1Brief(wrt, &tpd.Metadata)

		case storage.WebResourceV1:
			wrErr = encodeMetadataV1Brief(wrt, &tpd.Metadata)

		case *storage.WebResourceV1:
			wrErr = encodeMetadataV1Brief(wrt, &tpd.Metadata)

		default:
			return fmt.Errorf("%w: %+v", ErrDetectInputType, tpd)
		}

		if wrErr != nil {
			return wrErr
		}
	}

	return nil
}

// EncodeBrief encodes given resources as tab separated table containing Kind, Name, Source.
func EncodeBrief(w io.Writer, v ...interface{}) error {
	wrt := tabwriter.NewWriter(w, 1, 4, 1, ' ', tabwriter.Debug)

	if _, err := fmt.Fprintln(wrt, "Kind", "\t", "Name@Source"); err != nil {
		return err
	}

	if err := encodeResourceV1sBrief(wrt, v...); err != nil {
		return err
	}

	return wrt.Flush()
}
