package serializer

import "gitlab.com/accesscontroller/accesscontroller/controller/storage"

// mdWrapper wraps storage.MetadataV1 so as to be able to read it from JSON, YAML.
type mdWrapper struct {
	Metadata storage.MetadataV1
}
