package create

import (
	"errors"
	"io"

	"gitlab.com/accesscontroller/aclient/pkg/filereader"
	"gitlab.com/accesscontroller/aclient/pkg/printer"
	"gitlab.com/accesscontroller/cclient.v1"
)

var (
	ErrUnsupportedKind = errors.New("unsupported resource Kind")
	ErrTypeError       = errors.New("type cast error")
)

// Create creates given resources on a Server.
func Create(cl *cclient.CClient, outputFormat string, out io.Writer, inputFiles ...string) error {
	// Read input.
	resources, err := filereader.Read(inputFiles...)
	if err != nil {
		return err
	}

	// Make resources for cclient.
	var reqRess = make([]interface{}, len(resources))

	for i, v := range resources {
		reqRess[i] = v.Value
	}

	// Create.
	if _, err := cl.CreateResourcesV1(false, reqRess...); err != nil {
		return err
	}

	// Write output.
	if _, err := printer.Write(outputFormat, out, resources); err != nil {
		return err
	}

	return nil
}
