package printer

import (
	"fmt"
	"io"

	"gitlab.com/accesscontroller/aclient/pkg/info"
	"gitlab.com/accesscontroller/aclient/pkg/serializer"
)

// Write writes the given resources representation to the provided writer.
// Attention: always returns 0 as bytes written.
func Write(format string, w io.Writer, resources ...interface{}) (int, error) {
	// Check format.
	if err := ValidateOutputFormat(format); err != nil {
		return 0, err
	}

	// Print.
	switch format {
	case "":
		return 0, writeBrief(w, resources...)
	case OutJSON:
		return 0, writeJSON(w, resources...)

	case OutYAML:
		return 0, writeYAML(w, resources...)

	default:
		return 0, fmt.Errorf("%w: format %q", ErrUnsupportedOutputFormat, format)
	}
}

// makeFlatten checks the resource types and flattens possible slices.
func makeFlatten(resources ...interface{}) []interface{} {
	var res = make([]interface{}, 0, len(resources))

	for i := range resources {
		// Type Check and convert to flatted storage.* Resources.
		switch tpd := resources[i].(type) {
		case *info.Resource:
			res = append(res, tpd.Value)

		case info.Resource:
			res = append(res, tpd.Value)

		case []*info.Resource:
			for _, r := range tpd {
				res = append(res, r.Value)
			}

		case []info.Resource:
			for i := range tpd {
				res = append(res, tpd[i].Value)
			}

		default:
			// Write a "Generic" storage resource.
			res = append(res, tpd)
		}
	}

	return res
}

// writeYAML writes resources as YAML.
func writeYAML(w io.Writer, resources ...interface{}) error {
	return serializer.EncodeYAML(w, makeFlatten(resources...)...)
}

// writeJSON writes resources as JSON.
func writeJSON(w io.Writer, resources ...interface{}) error {
	return serializer.EncodeJSON(w, resources...)
}

// writeBrief writes resources as "brief" table.
func writeBrief(w io.Writer, resources ...interface{}) error {
	return serializer.EncodeBrief(w, resources...)
}
