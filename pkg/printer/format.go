package printer

import (
	"errors"
	"fmt"
	"strings"
)

const (
	// OutYAML defines Output YAML format.
	OutYAML = "yaml"

	// OutJSON defines Output JSON format.
	OutJSON = "json"
)

// ErrUnsupportedOutputFormat unsupported output format error.
var ErrUnsupportedOutputFormat = errors.New("output format is not supported")

// ValidateOutputFormat validates output format.
func ValidateOutputFormat(o string) error {
	// No format - default printer will be used.
	if o == "" {
		return nil
	}

	switch strings.ToLower(o) {
	case OutYAML:
		return nil
	case OutJSON:
		return nil
	default:
		return fmt.Errorf("%w: %q is not supported", ErrUnsupportedOutputFormat, o)
	}
}
