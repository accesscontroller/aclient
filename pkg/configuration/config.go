package configuration

import (
	"crypto/tls"
	"crypto/x509"
	"errors"
	"fmt"
	"net/url"
	"time"

	"github.com/spf13/viper"
)

var ErrIncorrectConfigValue = errors.New("incorrect configuration value")

// ParsedAPIServer contains parsed API Server configuration.
type ParsedAPIServer struct {
	InsecureConnection bool
	RequestTimeout     time.Duration
	URL                *url.URL
	CACert             *x509.CertPool
	Client             tls.Certificate
	APIKey             string
}

// ParsedConfig contains parsed Configuration.
type ParsedConfig struct {
	APIServer ParsedAPIServer
}

// APIServer contains raw API Server connection configuration.
type APIServer struct {
	InsecureConnection bool   `json:"insecureConnection,omitempty" yaml:"insecureConnection,omitempty"`
	RequestTimeoutSec  int    `json:"requestTimeoutSec,omitempty" yaml:"requestTimeoutSec,omitempty"`
	URL                string `json:"url,omitempty" yaml:"url,omitempty"`
	CACert             string `json:"caCert,omitempty" yaml:"caCert,omitempty"`
	ClientCert         string `json:"clientCert,omitempty" yaml:"clientCert,omitempty"`
	ClientKey          string `json:"clientKey,omitempty" yaml:"clientKey,omitempty"`
	APIKey             string `json:"accessToken,omitempty" yaml:"accessToken,omitempty"`
}

// Parse parses raw APIServer to ParseAPIServer.
func (r *APIServer) Parse() (*ParsedAPIServer, error) {
	var res ParsedAPIServer

	// Parse and validate.
	// InsecureConnection.
	res.InsecureConnection = r.InsecureConnection

	// RequestTimeout.
	// Min request timeout is 1 second.
	if r.RequestTimeoutSec < 1 {
		return nil, fmt.Errorf("%w: RequestTimeoutSec must be more than one second, got %d",
			ErrIncorrectConfigValue, r.RequestTimeoutSec)
	}

	res.RequestTimeout = time.Second * time.Duration(r.RequestTimeoutSec)

	// API Server URL.
	if r.URL == "" {
		return nil, fmt.Errorf("%w: empty URL is not allowed", ErrIncorrectConfigValue)
	}

	u, err := url.Parse(r.URL)
	if err != nil {
		return nil, fmt.Errorf("%w: URL is incorrect %q: %q",
			ErrIncorrectConfigValue, r.URL, err)
	}

	res.URL = u

	// CA Certificate.
	if r.CACert != "" {
		// Parse.
		ca, err := decodeCA(r.CACert)
		if err != nil {
			return nil, fmt.Errorf("%w: CA cert parse error: %s", ErrIncorrectConfigValue, err)
		}

		res.CACert = ca
	}

	// Client Certs.
	if r.ClientCert != "" && r.ClientKey != "" {
		// Parse.
		cert, err := decodeClientCerts(r.ClientCert, r.ClientKey)
		if err != nil {
			return nil, fmt.Errorf("%w: Client cert parse error: %s", ErrIncorrectConfigValue, err)
		}

		res.Client = cert
	}

	// Access Token.
	res.APIKey = r.APIKey

	return &res, nil
}

// Config contains raw aclient configuration.
type Config struct {
	APIServer APIServer `json:"apiServer,omitempty" yaml:"apiServer,omitempty"`
}

// Parse parses Config to ParsedConfig.
func (c *Config) Parse() (*ParsedConfig, error) {
	var cfg ParsedConfig

	// Parse API Server config.
	pAPI, err := c.APIServer.Parse()
	if err != nil {
		return nil, fmt.Errorf("parse APIServer configuration error: %w", err)
	}

	cfg.APIServer = *pAPI

	return &cfg, nil
}

// Parse parses Config using viper instance.
func Parse(vp *viper.Viper) (*ParsedConfig, error) {
	// Load.
	var rawCfg Config

	if err := vp.Unmarshal(&rawCfg); err != nil {
		return nil, fmt.Errorf("can not parse Configuration: %w", err)
	}

	// Parse.
	return rawCfg.Parse()
}
