package get

import (
	"errors"
	"io"

	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
	"gitlab.com/accesscontroller/aclient/pkg/printer"
	"gitlab.com/accesscontroller/cclient.v1"
)

var (
	ErrUnsupportedKind = errors.New("unsupported resource Kind")
	ErrTypeError       = errors.New("type cast error")
)

// Get loads resources from a Server and writes them to output.
func Get(cl *cclient.CClient, outputFormat string, out io.Writer, kind storage.ResourceKindT,
	source storage.ResourceNameT, names ...storage.ResourceNameT) error {
	// Load.
	resources, err := cl.GetResourcesV1(kind, source, names...)
	if err != nil {
		return err
	}

	// Write output.
	// Using resources... allows the printer print the resources as separate documents
	// in case of yaml.
	if _, err := printer.Write(outputFormat, out, resources...); err != nil {
		return err
	}

	return nil
}
