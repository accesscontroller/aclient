package delete

import (
	"errors"
	"io"

	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
	"gitlab.com/accesscontroller/aclient/pkg/filereader"
	"gitlab.com/accesscontroller/aclient/pkg/printer"
	"gitlab.com/accesscontroller/cclient.v1"
)

var (
	ErrUnsupportedKind = errors.New("unsupported resource Kind")
	ErrTypeError       = errors.New("type cast error")
)

// FromFiles deletes given resources on a Server.
func FromFiles(cl *cclient.CClient, outputFormat string, out io.Writer, inputFiles ...string) error {
	// Read input.
	resources, err := filereader.Read(inputFiles...)
	if err != nil {
		return err
	}

	// Make resources for cclient.
	var reqRess = make([]interface{}, len(resources))

	for i, v := range resources {
		reqRess[i] = v.Value
	}

	// Delete.
	if err := cl.DeleteResourcesV1(reqRess...); err != nil {
		return err
	}

	// Write output.
	if _, err := printer.Write(outputFormat, out, resources); err != nil {
		return err
	}

	return nil
}

// FromNames deletes resources using their identifying source, kind, names.
func FromNames(cl *cclient.CClient, outputFormat string, out io.Writer,
	kind storage.ResourceKindT, source storage.ResourceNameT, names ...storage.ResourceNameT) error {
	// Load Resources.
	apiResources, err := cl.GetResourcesV1(kind, source, names...)
	if err != nil {
		return err
	}

	// Delete resources.
	if err := cl.DeleteResourcesV1(apiResources...); err != nil {
		return err
	}

	// Print.
	if _, err := printer.Write(outputFormat, out, apiResources...); err != nil {
		return err
	}

	return nil
}
