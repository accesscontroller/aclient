package loadlists

import (
	"bufio"
	"crypto/sha256"
	"errors"
	"fmt"
	"io"
	"net/url"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
	"go.uber.org/zap"
)

const (
	lineBufferSize = 1024 * 16

	webResourceNameSep = "-"
)

var ErrBufferOverflow = errors.New("can not read a line from file. Line is too long")

func normalizeURL(line string) (*url.URL, error) {
	// Replace path slash characters.
	line = strings.ReplaceAll(line, "\\", "/")

	// Workaround for a case when a URL does not contain a scheme.
	if !strings.Contains(line, "://") {
		line = "http://" + line

		parsedURL, err := url.Parse(line)
		if err != nil {
			return nil, err
		}

		parsedURL.Scheme = ""

		return parsedURL, nil
	}

	parsedURL, err := url.Parse(line)
	if err != nil {
		return nil, err
	}

	return parsedURL, nil
}

// readResourcesListFile reads one file, creates WebResourceV1s, puts them into given map.
func readResourcesListFile(fn string, fileType fileTypeT,
	webResourceCategory storage.ResourceNameT, renamer renamerT,
	appendTo *rawResources,
) (*rawResources, error) {
	// Open File.
	fd, err := os.Open(fn)
	if err != nil {
		return nil, err
	}

	defer fd.Close()

	bio := bufio.NewReaderSize(fd, lineBufferSize)

	// Read.
	for {
		// Line.
		line, isPref, err := bio.ReadLine()
		if err != nil {
			if err != io.EOF {
				return nil, err
			}

			if len(line) == 0 {
				return appendTo, nil
			}
		}

		if isPref {
			return nil, ErrBufferOverflow
		}

		// WebResourceV1.
		switch fileType {
		// File contains domains.
		case domainsFile:
			newName := renamer(string(line) + webResourceNameSep + DomainsFileName)

			res := appendTo.getAddResource(newName)

			res.addDomain(storage.WebResourceDomainT(line))
			res.addCategory(webResourceCategory)

		// File contains URLs.
		case urlsFile:
			// Parse URL.
			uLine := string(line)

			parsedURL, err := normalizeURL(uLine)
			if err != nil {
				return nil, err
			}

			// I'm not sure that it is the best way but SHA256 seems to be not the worst
			// variant for url's.
			path := sha256.Sum256(line)

			newName := renamer(
				fmt.Sprintf("%s%s%x%s%s",
					parsedURL.Host,
					webResourceNameSep,
					string(path[:]),
					webResourceNameSep,
					URLsFileName))

			res := appendTo.getAddResource(newName)

			res.addURL(parsedURL)
			res.addCategory(webResourceCategory)

		// File contains IP addresses.
		case ipsFile:
			newName := renamer(string(line) + webResourceNameSep + IPsFileName)

			res := appendTo.getAddResource(newName)

			res.addIP(string(line))
			res.addCategory(webResourceCategory)

		default:
			return nil, fmt.Errorf("%w: fileType %d", ErrIncorrectFileType, fileType)
		}
	}
}

// readDir recursively reads given directory and processes files, directories as
// WebResourceV1s and WebResourceCategoryV1s respectively.
func readDir(rootDir string, renamer renamerT, logger *zap.Logger) (*rawCategories, *rawResources, error) {
	var (
		categories = newRawCategories()
		resources  = newRawResources()
	)

	// Read and process directory.
	if err := filepath.Walk(rootDir, func(path string, info os.FileInfo, err error) error {
		// Logger.
		pathLogger := logger.With(zap.String("path", path), zap.Error(err))

		// Return error on error without any further action.
		if err != nil {
			return err
		}

		// Logging.
		pathLogger.Info("Processing a path", zap.Bool("is_directory", info.IsDir()))

		// Process a file or directory.
		// Get SubPath.
		relPath, err := filepath.Rel(rootDir, path)
		if err != nil {
			return err
		}

		// Skip the rootDir.
		if relPath == "." {
			pathLogger.Debug("Skip the root directory")

			return nil
		}

		// WebResourceCategory.
		// Directories are WebResourceCategoryV1s.
		// relPath is used as the WebResourceCategoryV1 Name.
		if info.IsDir() {
			// Here the path is the path to a file so filepath.Dir(relPath) must give exactly the
			// WebResourceCategoryV1 Name.
			categoryName := storage.ResourceNameT(renamer(relPath))

			pathLogger.Debug("Using WebResourceCategoryV1 name",
				zap.String("WebResourceCategoryV1#Metadata#Name", string(categoryName)))

			categories.getAddCategory(categoryName)

			// Early exit.
			return nil
		}

		// In case of file parse we must delete the last part of the relPath.
		// Here the path is the path to a file so filepath.Dir(relPath) must give exactly the
		// WebResourceCategoryV1 Name.
		categoryName := storage.ResourceNameT(renamer(filepath.Dir(relPath)))

		pathLogger.Debug("Using WebResourceCategoryV1 name",
			zap.String("WebResourceCategoryV1#Metadata#Name", string(categoryName)))

		// Read a file and get WebResourceV1s.
		// Silently skip unsupported files.
		var fileType fileTypeT

		switch info.Name() {
		case DomainsFileName:
			fileType = domainsFile

			// Logging.
			pathLogger.Debug("Detected fileType", zap.String("file_type", DomainsFileName))

		case URLsFileName:
			fileType = urlsFile

			// Logging.
			pathLogger.Debug("Detected fileType", zap.String("file_type", URLsFileName))

		case IPsFileName:
			fileType = ipsFile

			// Logging.
			pathLogger.Debug("Detected fileType", zap.String("file_type", IPsFileName))

		default:
			// Logging.
			pathLogger.Debug("Detected unsupported fileType. Skip.", zap.String("file_type", info.Name()))

			return nil
		}

		// Read a file.
		resources, err = readResourcesListFile(path, fileType, categoryName, renamer, resources)
		if err != nil {
			return err
		}

		return nil
	}); err != nil {
		return nil, nil, err
	}

	return categories, resources, nil
}
