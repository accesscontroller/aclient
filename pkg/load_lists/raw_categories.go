package loadlists

import "gitlab.com/accesscontroller/accesscontroller/controller/storage"

type rawCategories struct {
	names map[storage.ResourceNameT]bool
}

func newRawCategories() *rawCategories {
	return &rawCategories{
		names: make(map[storage.ResourceNameT]bool),
	}
}

func (r *rawCategories) getAddCategory(c storage.ResourceNameT) {
	r.names[c] = true
}

func (r *rawCategories) getWebResourceCategories() map[storage.ResourceNameT]*storage.WebResourceCategoryV1 {
	var result = make(map[storage.ResourceNameT]*storage.WebResourceCategoryV1, len(r.names))

	for n := range r.names {
		var cat = storage.NewWebResourceCategoryV1(n)

		cat.Metadata.ETag = 1 // 1 is the default initial value.
		cat.Data.Description = storage.WebResourceCategoryDescriptionT(n)

		result[n] = cat
	}

	return result
}
