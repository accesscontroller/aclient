package loadlists

import (
	"errors"
	"fmt"
	"os"

	"regexp"

	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
	"gitlab.com/accesscontroller/cclient.v1"
	"go.uber.org/zap"
)

var (
	ErrIncorrectFileType = errors.New("incorrect file type. Can not load the file")
)

const (
	DomainsFileName = "domains"
	URLsFileName    = "urls"
	IPsFileName     = "ips"
)

type renamerT func(string) string

type fileTypeT int

const (
	domainsFile fileTypeT = 1
	urlsFile    fileTypeT = 2
	ipsFile     fileTypeT = 3
)

// makeRenamer creates a method to perform WebResourceCategoryV1 Name Renaming.
func makeRenamer(match, replace string) (renamerT, error) {
	// Do nothing.
	if match == "" {
		return func(s string) string { return s }, nil
	}

	// Compile Regex.
	rgx, err := regexp.Compile(match)
	if err != nil {
		return nil, fmt.Errorf("can not compile match Regexp: %w", err)
	}

	return func(s string) string {
		return rgx.ReplaceAllString(s, replace)
	}, nil
}

// syncWebResourceCategories performs WebResourceCategoryV1s synchronization between
// api and reference.
func syncWebResourceCategories(cl *cclient.CClient,
	ref, api map[storage.ResourceNameT]*storage.WebResourceCategoryV1,
	logger *zap.Logger) error {
	// Compute Diffs.
	logger.Debug("Computing WebResourceCategoryV1s diffs")

	deleteC, createC, updateC := computeWebResourceCategoriesDiff(ref, api)

	logger.Debug("Finished computing WebResourceCategoryV1 diffs",
		zap.Int("delete-count", len(deleteC)),
		zap.Int("create-count", len(createC)),
		zap.Int("update-count", len(updateC)),
	)

	// Perform actions.
	// deleteC.
	logger.Debug("Deleting WebResourceCategoryV1s")

	for i := range deleteC {
		if err := cl.DeleteWebResourceCategory(&deleteC[i]); err != nil {
			return err
		}
	}

	// createC.
	logger.Debug("Creating WebResourceCategoryV1s")

	if _, err := cl.CreateWebResourceCategories(createC, false); err != nil {
		return err
	}

	// updateC.
	logger.Debug("Updating WebResourceCategoryV1s")

	for i := range updateC {
		pc := &updateC[i]

		if _, err := cl.UpdateWebResourceCategory(pc.Metadata.Name, pc, false); err != nil {
			return err
		}
	}

	logger.Debug("Finished WebResourceCategoryV1s synchronization")

	return nil
}

// syncWebResources performs WebResourceV1s synchronization between
// api and reference.
func syncWebResources(cl *cclient.CClient,
	ref, api map[storage.ResourceNameT]*storage.WebResourceV1,
	logger *zap.Logger) error {
	// Compute Diffs.
	logger.Debug("Computing WebResourceV1s diffs")

	deleteC, createC, updateC := computeWebResourcesDiff(ref, api)

	logger.Debug("Finished computing WebResourceV1 diffs",
		zap.Int("delete-count", len(deleteC)),
		zap.Int("create-count", len(createC)),
		zap.Int("update-count", len(updateC)),
	)

	// Perform actions.
	// deleteC.
	logger.Debug("Deleting WebResourceV1s")

	for i := range deleteC {
		if err := cl.DeleteWebResource(&deleteC[i]); err != nil {
			return err
		}
	}

	// createC.
	logger.Debug("Creating WebResourceV1s")

	if _, err := cl.CreateWebResources(createC, false); err != nil {
		return err
	}

	// updateC.
	logger.Debug("Updating WebResourceV1s")

	for i := range updateC {
		pc := &updateC[i]

		if _, err := cl.UpdateWebResource(pc.Metadata.Name, pc, false); err != nil {
			return err
		}
	}

	logger.Debug("Finished WebResourceV1s synchronization")

	return nil
}

func Load(cl *cclient.CClient, logger *zap.Logger, simulate bool, rootDir, match, replace string) error {
	// Logging.
	logger = logger.With(
		zap.Bool("simulate", simulate),
		zap.String("root", rootDir),
	)

	logger.Debug("Loading WebResourceCategoryV1s and WebResourceV1s from directory")

	// Check dir.
	if _, err := os.Stat(rootDir); err != nil {
		return err
	}

	// Make WebResourceCategoryV1s renamer.
	renamer, err := makeRenamer(match, replace)
	if err != nil {
		return err
	}

	// Logging.
	logger.Debug("Starting reading directory")

	// Load all WebResourceCategoryV1s.
	refCats, refRess, err := readDir(rootDir, renamer, logger)
	if err != nil {
		return err
	}

	// Logging.
	logger.Debug("Finished reading directory. Got Resources",
		zap.Int("WebResourceCategoryV1.Count", len(refCats.names)),
		zap.Int("WebResourceV1.Count", len(refRess.resources)))

	// Load existing data.
	// Logging.
	logger.Debug("Starting loading resources from API Server")

	apiCategories, apiResources, err := loadFromAPI(cl, logger)
	if err != nil {
		return err
	}

	// Logging.
	logger.Debug("Finished loading resources from API Server. Got Resources",
		zap.Int("WebResourceCategoryV1.Count", len(apiCategories)),
		zap.Int("WebResourceV1.Count", len(apiResources)))

	logger.Debug("Starting WebResourceCategoryV1s synchronization")

	if !simulate {
		if err := syncWebResourceCategories(cl,
			refCats.getWebResourceCategories(),
			apiCategories,
			logger); err != nil {
			return err
		}
	}

	logger.Debug("Finished WebResourceCategoryV1s synchronization")

	logger.Debug("Starting WebResourceV1s synchronization")

	if !simulate {
		ress, err := refRess.getWebResources()
		if err != nil {
			return err
		}

		if err := syncWebResources(cl, ress, apiResources, logger); err != nil {
			return err
		}
	}

	logger.Debug("Finished WebResourceV1s synchronization")

	return nil
}
