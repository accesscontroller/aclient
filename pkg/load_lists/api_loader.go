package loadlists

import (
	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
	"gitlab.com/accesscontroller/cclient.v1"
	"go.uber.org/zap"
)

// loadWebResourceCategories loads all WebResourceCategoryV1s from API.
func loadWebResourceCategories(cl *cclient.CClient) (map[storage.ResourceNameT]*storage.WebResourceCategoryV1, error) {
	// Load.
	cats, err := cl.GetWebResourceCategories(nil)
	if err != nil {
		return nil, err
	}

	// Preprocess.
	result := make(map[storage.ResourceNameT]*storage.WebResourceCategoryV1, len(cats))

	for i := range cats {
		result[cats[i].Metadata.Name] = &cats[i]
	}

	return result, nil
}

// loadWebResources loads all WebResourceV1s from API.
func loadWebResources(cl *cclient.CClient) (map[storage.ResourceNameT]*storage.WebResourceV1, error) {
	// Load.
	resources, err := cl.GetWebResources(nil)
	if err != nil {
		return nil, err
	}

	// Preprocess.
	result := make(map[storage.ResourceNameT]*storage.WebResourceV1, len(resources))

	for i := range resources {
		result[resources[i].Metadata.Name] = &resources[i]
	}

	return result, nil
}

// loadFromAPI loads resources (WebResourceCategoryV1s, WebResourceV1s) from API.
func loadFromAPI(cl *cclient.CClient, logger *zap.Logger) (
	refCats map[storage.ResourceNameT]*storage.WebResourceCategoryV1,
	refRess map[storage.ResourceNameT]*storage.WebResourceV1,
	_ error) {
	// WebResourceCategories.
	logger.Debug("Starting loading WebResourceCategoryV1s from API")

	apiCategories, err := loadWebResourceCategories(cl)
	if err != nil {
		return nil, nil, err
	}

	logger.Debug("Completed loading WebResourceCategoryV1s from API", zap.Int("loaded_count", len(apiCategories)))

	// WebResources.
	logger.Debug("Starting loading WebResourceV1s from API")

	apiResources, err := loadWebResources(cl)
	if err != nil {
		return nil, nil, err
	}

	logger.Debug("Completed loading WebResourceV1s from API", zap.Int("loaded_count", len(apiResources)))

	return apiCategories, apiResources, nil
}
