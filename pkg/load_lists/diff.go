package loadlists

import (
	"fmt"

	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

// computeWebResourceCategoriesDiff computes updates for WebResourceCategoryV1s.
func computeWebResourceCategoriesDiff(refCategories, apiCategories map[storage.ResourceNameT]*storage.WebResourceCategoryV1) (
	deleteC []storage.WebResourceCategoryV1,
	createC []storage.WebResourceCategoryV1,
	updateC []storage.WebResourceCategoryV1,
) {
	// Delete lookup.
	for apiN, apiC := range apiCategories {
		_, ok := refCategories[apiN]
		if !ok {
			deleteC = append(deleteC, *apiC)
		}
	}

	// Create lookup.
	for refN, refC := range refCategories {
		apiC, ok := apiCategories[refN]
		if !ok {
			createC = append(createC, *refC)

			continue
		}

		// Check Update.
		// Here API WebResourceCategoryV1 exists. Let's check that the fields are equal.
		if apiC.Data.Description != refC.Data.Description {
			refC.Metadata.ETag = apiC.Metadata.ETag

			updateC = append(updateC, *refC)
		}
	}

	for _, v := range createC {
		fmt.Printf("Name: %q\n", v.Metadata.Name)
	}

	return deleteC, createC, updateC
}

// computeWebResourcesDiff computes difference between reference resources and API resources.
func computeWebResourcesDiff(refResources, apiResources map[storage.ResourceNameT]*storage.WebResourceV1) (
	deleteR []storage.WebResourceV1,
	createR []storage.WebResourceV1,
	updateR []storage.WebResourceV1,
) {
	// Delete lookup.
	for apiN, apiR := range apiResources {
		_, ok := refResources[apiN]
		if !ok {
			deleteR = append(deleteR, *apiR)
		}
	}

	// Create lookup.
	for refN, refR := range refResources {
		apiR, ok := apiResources[refN]
		if !ok {
			// Set initial ETag.
			refR.Metadata.ETag = 1

			createR = append(createR, *refR)

			continue
		}

		// Check Update.
		if !apiR.Equal(refR) {
			refR.Metadata = apiR.Metadata

			updateR = append(updateR, *refR)
		}
	}

	// fmt.Println("Delete")
	// domainsCount(deleteR)

	// fmt.Println("Update")
	// domainsCount(updateR)

	// fmt.Println("Create")
	// domainsCount(createR)

	return deleteR, createR, updateR
}

// func domainsCount(rsc []storage.WebResourceV1) {
// 	var (
// 		domains = make(map[storage.WebResourceDomainT]int)
// 		urls    = make(map[string]int)
// 		ips     = make(map[string]int)
// 	)

// 	for _, r := range rsc {
// 		for _, d := range r.Data.Domains {
// 			domains[d]++
// 		}

// 		for _, u := range r.Data.URLs {
// 			urls[u.String()]++
// 		}

// 		for _, ip := range r.Data.IPs {
// 			ips[ip.String()]++
// 		}
// 	}

// 	fmt.Println("Domains")

// 	for d, c := range domains {
// 		if c > 1 {
// 			fmt.Printf("%s: %d\n", d, c)
// 		}
// 	}

// 	fmt.Println("URLs")

// 	for u, c := range urls {
// 		if c > 1 {
// 			fmt.Printf("%s: %d\n", u, c)
// 		}
// 	}

// 	fmt.Println("IPs")

// 	for ip, c := range ips {
// 		if c > 1 {
// 			fmt.Printf("%s: %d\n", ip, c)
// 		}
// 	}
// }
