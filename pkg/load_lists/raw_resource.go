package loadlists

import (
	"errors"
	"fmt"
	"net"
	"net/url"

	"gitlab.com/accesscontroller/accesscontroller/controller/storage"
)

var ErrParseIP = errors.New("can not parse resource line as net.IP from a file")

type rawResource struct {
	name storage.ResourceNameT

	categories map[storage.ResourceNameT]bool

	urls    map[url.URL]bool
	domains map[storage.WebResourceDomainT]bool
	ips     map[string]bool
}

func newRawResource(name storage.ResourceNameT) *rawResource {
	return &rawResource{
		name:       name,
		categories: make(map[storage.ResourceNameT]bool),
		urls:       make(map[url.URL]bool),
		domains:    make(map[storage.WebResourceDomainT]bool),
		ips:        make(map[string]bool),
	}
}

func (r *rawResource) addCategory(c storage.ResourceNameT) {
	r.categories[c] = true
}

func (r *rawResource) addURL(u *url.URL) {
	r.urls[*u] = true
}

func (r *rawResource) addDomain(d storage.WebResourceDomainT) {
	r.domains[d] = true
}

func (r *rawResource) addIP(addr string) {
	r.ips[addr] = true
}

func (r *rawResource) getIPs() ([]net.IP, error) {
	var ips = make([]net.IP, 0, len(r.ips))

	for raw := range r.ips {
		p := net.ParseIP(raw)
		if p == nil {
			return nil, fmt.Errorf("%w: raw IP %q", ErrParseIP, raw)
		}

		ips = append(ips, p)
	}

	return ips, nil
}

func (r *rawResource) getURLs() []url.URL {
	var urls = make([]url.URL, 0, len(r.urls))

	for u := range r.urls {
		urls = append(urls, u)
	}

	return urls
}

func (r *rawResource) getDomains() []storage.WebResourceDomainT {
	var domains = make([]storage.WebResourceDomainT, 0, len(r.domains))

	for d := range r.domains {
		domains = append(domains, d)
	}

	return domains
}

func (r *rawResource) getCategories() []storage.ResourceNameT {
	var categories = make([]storage.ResourceNameT, 0, len(r.categories))

	for c := range r.categories {
		categories = append(categories, c)
	}

	return categories
}

func (r *rawResource) getWebResource() (*storage.WebResourceV1, error) {
	var res = storage.NewWebResourceV1(r.name)

	res.Metadata.ETag = 1 // 1 is a default init value.
	res.Data.Description = storage.WebResourceDescriptionT(r.name)
	res.Data.Domains = r.getDomains()

	ips, err := r.getIPs()
	if err != nil {
		return nil, err
	}

	res.Data.IPs = ips

	res.Data.URLs = r.getURLs()

	if len(res.Data.Domains) == 0 && len(res.Data.IPs) == 0 && len(res.Data.URLs) == 0 {
		return nil, fmt.Errorf("resource must have at least one of Domains, IPs, URLs filled. Dump +%v", res)
	}

	res.Data.WebResourceCategoryNames = r.getCategories()

	if len(res.Data.WebResourceCategoryNames) == 0 {
		return nil, fmt.Errorf("resource must have at least one record in WebResourceCategoryNames. Dump: %+v", res)
	}

	return res, nil
}

type rawResources struct {
	resources map[storage.ResourceNameT]*rawResource
}

func newRawResources() *rawResources {
	return &rawResources{
		resources: make(map[storage.ResourceNameT]*rawResource),
	}
}

func (r *rawResources) getAddResource(name string) *rawResource {
	var iName = storage.ResourceNameT(name)

	res, exists := r.resources[iName]
	if exists {
		return res
	}

	res = newRawResource(iName)

	r.resources[iName] = res

	return res
}

func (r *rawResources) getWebResources() (map[storage.ResourceNameT]*storage.WebResourceV1, error) {
	var result = make(map[storage.ResourceNameT]*storage.WebResourceV1, len(r.resources))

	for n, rr := range r.resources {
		r, err := rr.getWebResource()
		if err != nil {
			return nil, err
		}

		result[n] = r
	}

	return result, nil
}
